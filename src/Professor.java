import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Professor {
    private static final HashMap<String, String> _QA_MAP = new HashMap<>();

    public static void main(String[] args)
    {

        addQuestionAnswerPair(
                "Wie hoch ist Ruhememranpotenrial?",
                "-70 mV");
        addQuestionAnswerPair(
                "Welche Arten von Transport duch die Membran gibt es?",
                "Diffusion, aktive Transport, Phagozytose");
        addQuestionAnswerPair(
                "Was bestimmt die Geschwindigkeit von AP - Fortleitung?",
                "Myelisiert Axon oder nicht, Axon Durchmesser");

        String question = pickRandomQuestion();

        System.out.println(question);

        Scanner input = new Scanner(System.in);

        String userAnswer = input.nextLine();

        System.out.println(isAnswerCorrect(userAnswer, question));

    }

    private static String pickRandomQuestion()
    {
        String[] qaKeys = _QA_MAP.keySet().toArray(new String[0]);  // create an array from keys in hashmap
        return qaKeys[new Random().nextInt(qaKeys.length)];
    }

    private static boolean isAnswerCorrect(String userAnswer, String questionKey)
    {
        String correctAnswer = _QA_MAP.get(questionKey);
        return userAnswer.equals(correctAnswer);
    }

    private static void addQuestionAnswerPair(String question, String answer)
    {
        _QA_MAP.put(question, answer);

    }
}
